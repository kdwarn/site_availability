# Site Availability

This program will check a specified list of websites to see if they are available, including if they have a current TLS certificate.

## Installation and Setup

1. Clone this repo into a directory of your choosing.
2. Make the `check_sites` file executable: `chmod 755 check_sites`
3. Create a file named `config` with at least one website address in it. Here is an example with two sites:

```
#!/bin/bash
sites = (
  "https://example1.com"
  "https://example2.com"
)
```

## Usage

The program is designed to be run manually or via cron. To run manually, `cd` into the installed directory and run `./check_sites`. The results (success or failure) will be shown. Or set up a cronjob like so:

`30 * * * * cd /path/to/installed/directory && ./check_sites > /dev/null`

Created like that, cron will only send system emails if there has been an error.

## Testing

This uses the [bats-core](https://github.com/bats-core/bats-core) testing framework and appears to catch at least the most obvious problems with site availability, but you may want to verify this on your own. See the `bats-core` documentation for information on installation and running the tests.
