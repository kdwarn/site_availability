#!/bin/bash

setup() {
  load 'test_helper/bats-support/load'
  load 'test_helper/bats-assert/load'
}

can_run_our_script() { # @test
  ./check_sites
}

starting_script_in_output() { # @test
  run ./check_sites
  assert_output --partial 'Starting check...'
}

verify_sites_var_exits_if_no_sites() { # @test
  . functions
  local sites=""
  run verify_sites_var "${sites[*]}"
  assert_output --partial 'No sites found to be checked.'
  assert_failure 1
}

verify_sites_var_works_if_array() { # @test
  . functions
  local sites=( "https://httpstat.us/200" "https://httpstat.us/302" "https://httpstat.us/201" )
  run verify_sites_var "${sites[*]}"
  assert_success
}

verify_sites_var_works_if_string() { # @test
  . functions
  local sites="test"
  run verify_sites_var "${sites[*]}"
  assert_success
}

get_http_header_exits_60_with_expired_ssl() { # @test
  . functions
  run get_http_header "https://expired.badssl.com/"
  assert_failure 60
}

get_http_header_exits_60_with_wrong_host() { # @test
  . functions
  run get_http_header "https://wrong.host.badssl.com/"
  assert_failure 60
}

get_http_header_exits_0_with_valid_ssl() { # @test
  . functions
  run get_http_header "https://sha256.badssl.com/"
  assert_success
}

get_http_header_exits_0_with_http() { # @test
  . functions
  run get_http_header "http://http.badssl.com/"
  assert_success
}

extract_response_returns_correct_value() { # @test
  . functions
  response=$( extract_response "$( get_http_header "https://httpstat.us/200" )" )
  assert_equal "$response" 200
  response=$( extract_response "$( get_http_header "https://httpstat.us/404" )" )
  assert_equal "$response" 404
  response=$( extract_response "$( get_http_header "https://httpstat.us/502" )" )
  assert_equal "$response" 502
  response=$( extract_response "$( get_http_header "https://httpstat.us/522" )" )
  assert_equal "$response" 522
}

check_under_400_sites() { # @test
  . functions
  local sites=( "https://httpstat.us/200" "https://httpstat.us/302" "https://httpstat.us/201" )
  run check_all_sites "${sites[*]}"
  assert_success
}  

check_list_of_sites_with_one_failure_and_one_success() { # @test
  . functions
  local sites=( 
    "https://httpstat.us/200" 
    "https://expired.badssl.com/"
  )
  run check_all_sites "${sites[*]}"
  assert_output --partial "200 SUCCESS"
  assert_failure
}
